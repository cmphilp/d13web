package com.db.grad.d13.context;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.Test;

import com.db.grad.d13.context.AutowiredServlet;
import com.db.grad.d13.context.AutowiredServletDependency;

@SuppressWarnings("serial")
public class AutowiredServletTest extends AutowiredServlet {	
	Integer nonInjectedInteger;
	@AutowiredServletDependency("test1")
	Integer injectedInteger;
	Character nonInjectedChar;
	
	Map<String, Object> testBeans = new HashMap<String, Object>(){{
		put("test1", new Integer(1337));
		put("test2", "not gonna trigger anything");
		put("test3", new Long(0));
	}};
	
	@Test
	public void autowireTest() {
		autowire(testBeans);
		assertNull(nonInjectedInteger);
		assertNull(nonInjectedChar);
		assertEquals(injectedInteger, new Integer(1337));
	}	
	
	@Test
	public void searchTest() {
		assertEquals(new Integer(1337), findBean("test1", testBeans));
		assertEquals("not gonna trigger anything", findBean("test2", testBeans));
		assertEquals(new Long(0), findBean("test3", testBeans));
	}
	
	@Test(expected = NoSuchElementException.class)
	public void searchFailTest() {
		findBean("noSuchElement", testBeans);
	}
	
}