package com.db.grad.d13.context;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextInitializer implements ServletContextListener {
	public static final String CONTEXT_NAME = "autowiredServletContext";
	
	public void contextInitialized(ServletContextEvent event) {	
		ServletContext sc = event.getServletContext();
		sc.setAttribute(CONTEXT_NAME, new ApplicationContext().context);
	}
	
	public void contextDestroyed(ServletContextEvent event) {}
}