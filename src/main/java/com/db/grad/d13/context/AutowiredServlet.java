package com.db.grad.d13.context;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import com.db.grad.d13.api.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("serial")
public abstract class AutowiredServlet extends HttpServlet { 
	
    @Override
    public final void init() throws ServletException {
        @SuppressWarnings("unchecked")
		Map<String, Object> context = (Map<String, Object>)getServletContext().getAttribute(ContextInitializer.CONTEXT_NAME);      
        
        autowire(context);
    }
    
    protected void autowire(Map<String, Object> context) {
    	Class<?> classForWiring = getClass();
    	while (!classForWiring.equals(HttpServlet.class)) {
	    	for(Field field : classForWiring.getDeclaredFields()){
		        if (field.isAnnotationPresent(AutowiredServletDependency.class)) {
		        	try {
		        		String beanName = field.getAnnotation(AutowiredServletDependency.class).value();
						field.set(this, findBean(beanName, context));
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
		        }	        	
	        }
	    	classForWiring = classForWiring.getSuperclass();
    	}
    }
    
    protected Object findBean(String name, Map<String, Object> context) {
    	return context.entrySet().stream().filter(x->x.getKey().equals(name)).findFirst().get().getValue();
    }
    
    protected void respondWithMessage(HttpServletResponse response, Message message) throws IOException{
		String serialized;
		try {
			serialized = new ObjectMapper().writeValueAsString(message);
	    	respondWithJson(response, serialized);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
    }
    
    private void respondWithJson(HttpServletResponse response, String data) throws IOException {
		response.setContentType("application/json;charset=utf-8");
    	PrintWriter out = response.getWriter();
		try {
			out.print(data);
	    } finally {
	    	out.close();
		}	
    }
}