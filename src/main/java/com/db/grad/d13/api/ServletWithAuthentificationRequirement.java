package com.db.grad.d13.api;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.grad.d13.context.AutowiredServlet;
import com.db.grad.d13.context.AutowiredServletDependency;
import com.db.grad.d13.core.login.LoginService;

@SuppressWarnings("serial")
public abstract class ServletWithAuthentificationRequirement extends AutowiredServlet { 
	private final static Message errorMessage = new Message("sessionError");

	@AutowiredServletDependency("loginService")
    public LoginService loginService;
	
	@Override
	public final void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException {
		Cookie[] cookies = request.getCookies();
        if (cookies != null) {
	        Optional<Cookie> credentialsCookie = Arrays.asList(cookies).stream()
		        .filter(e->LoginServlet.COOKIE_NAME.equals(e.getName()))
		        .findAny();
	        
	        if (!credentialsCookie.isPresent() || 
	        	!checkCookieData(credentialsCookie.get())) {
	        	System.out.println("Declined servlet request for servlet " + this.getClass());
	        	respondWithMessage(response, errorMessage);
	        	return;
	        }

        	System.out.println("Accepted servlet request for servlet " + this.getClass());
	        processRequest(request, response, 
	        		retrieveCookieData(credentialsCookie.get()).get("login"));
        } else {
        	respondWithMessage(response, errorMessage);
        }
	}
	
	private Map<String, String> retrieveCookieData(Cookie cookie){
		Map<String, String> map = new HashMap<>();
		String data = cookie.getValue();
		int delimiterIndex = data.indexOf(LoginServlet.CREDENTIAL_DELIMITER);
		map.put("login", data.substring(0, delimiterIndex));
		map.put("password", data.substring(delimiterIndex+1));
		return map;
	}
	
	private boolean checkCookieData(Cookie cookie) throws IOException {
		Map<String, String> cookieData = retrieveCookieData(cookie);
		return loginService.checkPermission(cookieData.get("login"), 
				cookieData.get("password")).getField("result").equals(LoginService.SUCCESS);
	}
	
	public abstract void processRequest(HttpServletRequest request, HttpServletResponse response, String username) throws IOException;
}