package com.db.grad.d13.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.grad.d13.context.AutowiredServlet;
import com.db.grad.d13.context.AutowiredServletDependency;
import com.db.grad.d13.core.database.DBStatusService;

@SuppressWarnings("serial")
@WebServlet("/dbstatus")
public class DBStatusServlet extends AutowiredServlet {
	
	@AutowiredServletDependency("dbStatusService")
    public DBStatusService dbStatusService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Message result = dbStatusService.checkDBConnection(); 
		respondWithMessage(resp, result);
	}	
}
