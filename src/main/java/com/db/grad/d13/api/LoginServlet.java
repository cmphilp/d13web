package com.db.grad.d13.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.grad.d13.context.AutowiredServlet;
import com.db.grad.d13.context.AutowiredServletDependency;
import com.db.grad.d13.core.login.LoginService;
import com.db.grad.d13.core.util.HashUtil;

@SuppressWarnings("serial")
@WebServlet("/login")
public class LoginServlet extends AutowiredServlet { 
	public static final String COOKIE_NAME = "d13credentials";
	public static final String CREDENTIAL_DELIMITER = ":";
	
	@AutowiredServletDependency("loginService")
    public LoginService loginService;
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException {
		  
		  String login = request.getParameter("login");
    	  String password = request.getParameter("password");
    	  password = HashUtil.hashPasswordToHex(password);
    	  
    	  Message result = loginService.checkPermission(login, password);    
		  
	      if (result.getField("result").equals(loginService.SUCCESS)) {
	          Cookie creds = new Cookie(COOKIE_NAME, login + CREDENTIAL_DELIMITER + password);
	          response.addCookie(creds);      	 
          }  	
	      
		  respondWithMessage(response, result);		  
	   }
}