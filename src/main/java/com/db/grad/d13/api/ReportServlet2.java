package com.db.grad.d13.api;

import com.db.grad.d13.context.AutowiredServletDependency;
import com.db.grad.d13.core.reports.Report;
import com.db.grad.d13.core.reports.ReportService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;


@SuppressWarnings("serial")
@WebServlet("/report2")
public class ReportServlet2 extends ServletWithAuthentificationRequirement {
    @AutowiredServletDependency("reportService")
    public ReportService reportService;

	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response, String username) throws IOException {
		Map<String, String[]> rawParameterMap = request.getParameterMap();
		Map<String, String> parameters = rawParameterMap.keySet().stream()
				.collect(Collectors.toMap(x->x, x->rawParameterMap.get(x)[0]));
		respondWithMessage(response, reportService.viewReport(parameters));
	}
}
