package com.db.grad.d13.api;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.grad.d13.context.AutowiredServlet;
import com.db.grad.d13.context.AutowiredServletDependency;
import com.db.grad.d13.core.login.LoginServiceImpl;

@SuppressWarnings("serial")
@WebServlet("/testCookie")
public class ServletForCookieTesting extends ServletWithAuthentificationRequirement {

	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response, String username) throws IOException {
		respondWithMessage(response, 
				new Message("testCookie")
					.setField("result", "success")
					.setField("username", username));
	} 
}