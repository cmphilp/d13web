var reportMetadatasMap;

function buildFilter(name, type) {
    if (type === "string")
        return "<br>"
            + "<input class='filterSubstring form-control' type='text' value='' name='"
            + name + "' placeholder='Filter by Substring'>";
    if (type === "number")
        return "<span class='numericFilterSpan'>"
            + "<span class='filterSpan'></span><input class='filterFrom form-control' type='number' value='' name='"
            + name
            + "_from' placeholder='Filter from...'>"
            + "<span class='filterSpan'></span><input class='filterTo form-control' type='number' value='' name='"
            + name + "_to' placeholder='Filter to...'>" + "</span>";
    return type + "- no filter";
}

function getTdHeaderName(td) {
    var index = td.parent().children().index(td);
    return $($("#reportTableHeader").children()[index]).text();
}

function getHeaderIndex(headerName) {
    var toReturn = -1;
    $("#reportTableHeader > td").each(function(index, value) {
        if (headerName == $(value).text()) {
            toReturn = index;
            return index;
        }
    });
    return toReturn;
}

function queryForTableData(reportName){
	var repMetadata = reportMetadatasMap[reportName];
	var sortColumn;
	var sortDirection;
	var page = parseInt($("#page").text());
		
	$(".sorting").each(
	    function(index, value) {
	        if ($(value).html()!="") {
	            sortColumn = getTdHeaderName($(value));
	            sortDirection = $(value).html();
	        }
	    }
	);
		
	var dataForSending = {};
	dataForSending["sortColumn"] = sortColumn;
	dataForSending["reportName"] = reportName;
	dataForSending["page"] = page;
	dataForSending["sortDirection"] = sortDirection;
	$(".filterSubstring").each(
	    function(index, value) {
	        if ($(value).val() !== "") {
	            dataForSending["filter_"+ getTdHeaderName($(value).parent())] = $(value).val();
	        }
	    }
	);
		
	$(".numericFilterSpan").each(
	    function(index, value) {
	        var from = $($(value).children(".filterFrom")[0]).val();
	        var to = $($(value).children(".filterTo")[0]).val();
	        if (from !== "" && to !== "")
	            dataForSending["filter_" + getTdHeaderName($(value).parent())] = from + ";" + to;
	    }
	);
		
	$.get(
	    "./report2",
	    dataForSending,
	    function(data) {
	        console.log(data);
		
	        if (data.messageType == "sessionError") {
	            showLoginForm();
	            return;
	        }
		
	        $("#tablebody").html("");
		
	        for ( var rowIndex in data.data) {
	            var dataForPopulating = [];
	            var rowData = data.data[rowIndex];
	            for ( var prop in rowData) {
	                dataForPopulating[getHeaderIndex(prop)] = rowData[prop];
	            }
		
	            var row = "<tr>";
	            for (var i = 0; i < dataForPopulating.length; i++) {
	                row += "<td>"
	                    + dataForPopulating[i]
	                    + "</td>";
	            }
	            row += "</tr>";
		
	            $("#tablebody").append($(row));
	        }
	        
	        if (data.requestedPage == 0) {
	        	$("#decrease").hide();
	        } else {
	        	$("#decrease").show();
	        }
	        if (data.hasMore == "false") {
	        	$("#increase").hide();
	        } else {
	        	$("#increase").show();
	        }
	    }
	);
}


function chooseSorting(clickedCheckbox) {
	var previousValue = $(clickedCheckbox).html();
    $(".sorting").each(function(index, value) {
        $(this).html("");
    });
    if (previousValue == "Ascending") {
    	$(clickedCheckbox).html("Descending");
    } else {
    	$(clickedCheckbox).html("Ascending");
    }
}

function chooseReport(reportName) {
    var repMetadata = reportMetadatasMap[reportName];
     if(repMetadata.reportType == "EPLV") {
    	d3.select("#displayAreaSpan svg").remove();
    	$("#displayAreaSpan").html(
	    	"<input type='submit' id='EPLVSubmit'>" +
	    	"<span id='visualizationArea'></span>"
    	);
    	
    	var dataForGettingCounterpartyList = {};
		dataForGettingCounterpartyList["sortColumn"] = "counterparty_name";
		dataForGettingCounterpartyList["reportName"] = "Counterparty List";
		dataForGettingCounterpartyList["page"] = 0;
		dataForGettingCounterpartyList["sortDirection"] = "ASC";
		      
		$.get("./report2",
			dataForGettingCounterpartyList,
			function(data) {
				if (data.messageType == "sessionError") {
		        	showLoginForm();
		        	return;
		        }	
		        console.log(data);  
		        var selector = "Choose counterparty to visualize: ";
		        selector += "<select id='counterpartySelector'>";
		        for (var i in data.data) {
		        	selector += "<option value='" + data.data[i]["counterparty_name"] + "'>" + 
		        				data.data[i]["counterparty_name"] + "</option>";
		        }
		        selector += "</select>";
		        $("#displayAreaSpan").prepend(selector);
		        
    		});
    	
    	var dataForGettingInstrumentList = {};
		dataForGettingInstrumentList["sortColumn"] = "instrument_name";
		dataForGettingInstrumentList["reportName"] = "Instrument List";
		dataForGettingInstrumentList["page"] = 0;
		dataForGettingInstrumentList["sortDirection"] = "ASC";
		
		var checkedOptions;
		      
		$.get("./report2",
			dataForGettingInstrumentList,
			function(data) {
				if (data.messageType == "sessionError") {
		        	showLoginForm();
		        	return;
		        }	
		        console.log(data);  
		        var selector = "<br>Choose instrument to visualize: ";
		        
		        for (var i in data.data) {
		        	selector += "<label><input type='checkbox' class='instrumentSelectorCheckbox' value='" + 
		        				data.data[i]["instrument_name"] + "' />" + data.data[i]["instrument_name"] + " </label> ";
		        }
		        selector += "<br>";
		        $("#displayAreaSpan").prepend(selector);
		        
    		});
    	
    	$('#EPLVSubmit').on('click', function() {
    		if ($("#counterpartySelector").val() == "") {
    			alert("Wrong arguments");
    			return;
    		}
    		
    		checkedOptions = [];
    		$(".instrumentSelectorCheckbox").each(function(index, value) {
		        if($(value).is(':checked')) {
		        	checkedOptions.push($(value).val());
		        }
		    });
		    console.log(checkedOptions);
    		
    		d3.select("#displayAreaSpan svg").remove();    		
    		$("#graphLegend").remove();
    			
    		$("#displayAreaSpan")
    			.append(
	    			"<span id='graphLegend'><br>" + 
	    				"Legend: " +
	    				"<span style='background-color:#F39C11; color:white; padding: 3px;'>Sell amount</span>" +
	    				" " +
	    				"<span style='background-color:#2A80B9; color:white; padding: 3px;'>Buy amount</span>" +
	    				" " +
	    				"<span style='background-color:#E84C3D; color:white; padding: 3px;'>Realized loss</span>" +
	    				" " +
	    				"<span style='background-color:#2DCC70; color:white; padding: 3px;'>Realized profit</span>" +
	    			"<br/></span>"
    			);
    		
	    	var dataForSending = {};
		    dataForSending["sortColumn"] = repMetadata.defaultSortColumn;
		    dataForSending["reportName"] = repMetadata.name;
		    dataForSending["page"] = 0;
		    dataForSending["sortDirection"] = "ASC";
		    dataForSending["filter_Counterparty"] = $("#counterpartySelector").val();
		      
		    $.get("./report2",
		    	dataForSending,
		    	function(data) {
		    		if (data.messageType == "sessionError") {
		            	showLoginForm();
		            	return;
		            }	
		            console.log("--->" + data);  
		            
                    var div = d3.select("body").append("div")
                            .attr("class", "tooltip")
                            .style("opacity", 0);	
                
		            var svg = d3.select("#displayAreaSpan").append("svg")
                            .attr("width", 1050)
                            .attr("height", 500)
                            .style("font", "10px sans-serif")
                            .style("text-align", "center")
                            .style("display", "block")
                            .style("margin-left", "auto")
                            .style("margin-right", "auto"),
                    margin = {
					    top: 20,
					    right: 20,
					    bottom: 30,
					    left: 40
					},
                    width = +svg.attr("width") - margin.left - margin.right,
  					height = +svg.attr("height") - margin.top - margin.bottom;
                    
                    var color = d3.scaleOrdinal(d3.schemeCategory10);

					var x = d3.scaleBand().rangeRound([0, width])
					  .padding(0.1),
					  y = d3.scaleLinear().rangeRound([height, 0]);
					
					var g = svg.append("g")
					  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
					
					var dataToVisualize = [];
								
					for (var row in data.data) {
						if (checkedOptions.indexOf(data.data[row]["Instrument"]) == -1) {
							continue;
						}
						var visualizationElement = {};
						visualizationElement["Group"] = "Realized profit/loss";
						visualizationElement["amount"] = data.data[row]["Effective profit/loss"];
						visualizationElement["instrument"] = data.data[row]["Instrument"];
						dataToVisualize.push(visualizationElement);
					}	
					
					for (var row in data.data) {
						if (checkedOptions.indexOf(data.data[row]["Instrument"]) == -1) {
							continue;
						}
						var visualizationElement = {};
						visualizationElement["Group"] = "Buy amount";
						visualizationElement["amount"] = data.data[row]["Buy amount"];
						visualizationElement["instrument"] = data.data[row]["Instrument"];
						dataToVisualize.push(visualizationElement);
					}	
					
					for (var row in data.data) {
						if (checkedOptions.indexOf(data.data[row]["Instrument"]) == -1) {
							continue;
						}
						var visualizationElement = {};
						visualizationElement["Group"] = "Sell amount";
						visualizationElement["amount"] = data.data[row]["Sell amount"];
						visualizationElement["instrument"] = data.data[row]["Instrument"];
						dataToVisualize.push(visualizationElement);
					}	
					
					var ymaxdomain = d3.max(dataToVisualize, function(d) {
					  return d.amount;
					});
					x.domain(dataToVisualize.map(function(d) {
					  return d.instrument
					}));
					y.domain([0, ymaxdomain]);
					
					var x1 = d3.scaleBand()
					  .rangeRound([0, x.bandwidth()])
					  .padding(0.05)
					  .domain(dataToVisualize.map(function(d) {
					    return d.Group;
					  }));
					
					color.domain(dataToVisualize.map(function(d) {
					  return d.Group;
					}));
					
					var groups = g.selectAll(null)
					  .data(dataToVisualize)
					  .enter()
					  .append("g")
					  .attr("transform", function(d) {
					    return "translate(" + x(d.instrument) + ",0)";
					  })
					
					var bars = groups.selectAll(null)
					  .data(function(d) {
					    return [d]
					  })
					  .enter()
					  .append("rect")
					  .attr("x", function(d, i) {
					    return x1(d.Group)
					  })
                      .on("mouseover", function(d) {
                           div.transition()
                             .duration(200)
                             .style("opacity", .9);
                           div.html(d.Group + "<br/>" + d.amount)
                             .style("left", (d3.event.pageX) + "px")
                             .style("top", (d3.event.pageY - 28) + "px");
                       })
                     .on("mouseout", function(d) {
                           div.transition()
                             .duration(500)
                             .style("opacity", 0);
                       })
					  .attr("y", function(d) {
					    return y(Math.abs(d.amount));
					  })
					  .attr("width", x1.bandwidth())
					  .attr("height", function(d) {
					    return height - y(Math.abs(d.amount));
					  })
					  .attr("fill", function(d) {
						  	if (d.Group == "Sell amount") {
						  		return "#F39C11";							  	
					  		} else if (d.Group == "Buy amount") {
					  			return "#2A80B9";	
					  		} else {
					  			if(d.amount > 0) {
							    	return "#2DCC70";
							    } else {
							    	return "#E84C3D";
							    }
					  		}		  	
					  });
					
					g.append("g")
                      .style("font-size", "16px")
					  .attr("class", "axis")
					  .attr("transform", "translate(0," + height + ")")
					  .call(d3.axisBottom(x));
					
					g.append("g")
					  .attr("class", "axis")
                      .style("font-size", "14px")
					  .call(d3.axisLeft(y).ticks(null, "s"))
					  .append("text")
					  .attr("x", 2)
					  .attr("y", y(y.ticks().pop()) + 0.5)
					  .attr("dy", "0.32em")
					  .attr("fill", "#000")
					  .attr("font-weight", "bold")
					  .attr("text-anchor", "start")
					  .text("amount");  
		        	}
	        ); 
    	}); 
	        
    } else if(repMetadata.reportType == "glare") {
        $("#displayAreaSpan").html("");
        var dataForSending = {};
        dataForSending["sortColumn"] = repMetadata.defaultSortColumn;
        dataForSending["reportName"] = repMetadata.name;
        dataForSending["page"] = 0;
        dataForSending["sortDirection"] = "ASC";
        $.get(
            "./report2",
            dataForSending,
            function(data) {
                //console.log(data);
            	if (data.messageType == "sessionError") {
                        showLoginForm();
                        return;
                    }
                var nodeData = {"name": "", "children": []};
                var instrument_name = "";
                var instrument_index = -1;
                var instruments_lst = [];
                for (var i in data.data) {
                    var cp = data.data[i];
                    if (instrument_name != cp.instrument_name) {
                        instrument_name = cp.instrument_name;
                        instruments_lst.push(instrument_name);
                        var node = {};
                        node.name = instrument_name;
                        node.children = [];
                        node.children.push({"name": cp.counterparty_name, "size": cp.sum});
                        nodeData.children.push(node);
                        instrument_index += 1;
                    } else{
                        nodeData.children[instrument_index].children.push({"name": cp.counterparty_name, "size": cp.sum});
                    }

                }

                //https://beta.observablehq.com/@mbostock/d3-zoomable-sunburst   
                
                var width = 800;
                var arc = d3.arc()
                    .startAngle(d => d.x0)
                    .endAngle(d => d.x1)
                    .padAngle(d => Math.min((d.x1 - d.x0) / 2, 0.005))
                    .padRadius(radius * 1.5)
                    .innerRadius(d => d.y0 * radius)
                    .outerRadius(d => Math.max(d.y0 * radius, d.y1 * radius - 1));

                var radius = width / 6;

                var format = d3.format(",d");
                
                var color = d3.scaleOrdinal().range(d3.quantize(d3.interpolateRainbow, nodeData.children.length + 1))
                var partition = data => {
                var root = d3.hierarchy(nodeData)
                      .sum(d => d.size)
                      .sort((a, b) => b.value - a.value);
                  return d3.partition()
                      .size([2 * Math.PI, root.height + 1])
                    (root);
                };
                

              const root = partition(data);

              root.each(d => d.current = d);

              var svg = d3.select("#displayAreaSpan").append("svg")
                            .attr("width", width)
                            .attr("height", width)
                            .style("font", "13px sans-serif")
                            .style("text-align", "center")
                            .style("display", "block")
                            .style("margin-left", "auto")
                            .style("margin-right", "auto");

              var g = svg.append("g")
                  .attr("transform", "translate(" + width / 2 + "," + (width/ 2) + ")");

              var path = g.append("g")
                .selectAll("path")
                .data(root.descendants().slice(1))
                .enter().append("path")
                  .attr("fill", d => { while (d.depth > 1) d = d.parent; return color(d.data.name); })
                  .attr("fill-opacity", d => arcVisible(d.current) ? (d.children ? 0.6 : 0.4) : 0)
                  .attr("d", d => arc(d.current))
                  .attr("stroke", "#fff")
                  .attr("stroke-width", 1.5)
                  .on("mouseover", mouseovered)
                  .on("mouseout", mouseouted);

              path.filter(d => d.children)
                  .style("cursor", "pointer")
                  .on("click", clicked);

              path.append("title")
                  .text(d => `${d.ancestors().map(d => d.data.name).reverse().join("/")}\n${format(d.value)}`);

              var label = g.append("g")
                  .attr("pointer-events", "none")
                  .attr("text-anchor", "middle")
                  .style("user-select", "none")
                  .selectAll("text")
                  .data(root.descendants().slice(1))
                  .enter().append("text")
                  .attr("dy", "0.35em")
                  .attr("fill-opacity", d => +labelVisible(d.current))
                  .attr("transform", d => labelTransform(d.current))
                  .text(d => d.data.name);

              var parent = g.append("circle")
                  .datum(root)
                  .attr("r", radius)
                  .attr("fill", "none")
                  .attr("pointer-events", "all")
                  .on("click", clicked);
              
              function mouseovered(p) {
                    //console.log(p);
                    var instruments =   
                        g.append("text")
                        .attr("fill", "black")
                        .attr("class", "lbl")
                        .text(function(){
                            if (p.height == 1) {
                                return p.data.name;
                            }
                            else {
                                return p.parent.data.name;
                            }
                        })
                        .attr("dy", "-20px");   
                        g.append("text")
                          .text("INSTRUMENT")
                          .attr("class", "lbl2")
                          .attr("dy", "-60px");
                  if (p.height == 0) {
                        g.append("text")
                        .attr("fill", "black")
                        .attr("class", "lbl")
                        .text(p.data.name)
                        .attr("dy", "65px")
                         g.append("text")
                          .text("COUNTERPARTY")
                          .attr("class", "lbl2")
                          .attr("dy", "25px");                     
                      
                  }
              }
              
              function mouseouted(p) {
                  d3.selectAll(".lbl").remove();
                  d3.selectAll(".lbl2").remove();

              }
                
              function clicked(p) {
                parent.datum(p.parent || root);

                root.each(d => d.target = {
                  x0: Math.max(0, Math.min(1, (d.x0 - p.x0) / (p.x1 - p.x0))) * 2 * Math.PI,
                  x1: Math.max(0, Math.min(1, (d.x1 - p.x0) / (p.x1 - p.x0))) * 2 * Math.PI,
                  y0: Math.max(0, d.y0 - p.depth),
                  y1: Math.max(0, d.y1 - p.depth)

                });

                const t = g.transition().duration(750);

                // Transition the data on all arcs, even the ones that aren’t visible,
                // so that if this transition is interrupted, entering arcs will start
                // the next transition from the desired position.
                path.transition(t)
                    .tween("data", d => {
                      const i = d3.interpolate(d.current, d.target);
                      return t => d.current = i(t);
                    })
                  .filter(function(d) {
                    return +this.getAttribute("fill-opacity") || arcVisible(d.target);
                  })
                    .attr("fill-opacity", d => arcVisible(d.target) ? (d.children ? 0.6 : 0.4) : 0)
                    .attrTween("d", d => () => arc(d.current));

                label.filter(function(d) {
                    return +this.getAttribute("fill-opacity") || labelVisible(d.target);
                  }).transition(t)
                    .attr("fill-opacity", d => +labelVisible(d.target))
                    .attrTween("transform", d => () => labelTransform(d.current));
              }

              function arcVisible(d) {
                return d.y1 <= 3 && d.y0 >= 1 && d.x1 > d.x0;
              }

              function labelVisible(d) {
                return d.y1 <= 3 && d.y0 >= 1 && (d.y1 - d.y0) * (d.x1 - d.x0) > 0.03;
              }

              function labelTransform(d) {
                const x = (d.x0 + d.x1) / 2 * 180 / Math.PI;
                const y = (d.y0 + d.y1) / 2 * radius;
                return `rotate(${x - 90}) translate(${y},0) rotate(${x < 180 ? 0 : 180})`;
              }

            }
        );
    } else {
        d3.select("#displayAreaSpan svg").remove();
        $("#displayAreaSpan").html(
            "<table class='table table-striped table-hover' id='reportTable' class='table table-bordered'>" + 
                "<thead class='thead-dark'>"
                    + "<tr id='reportFilters'>" + "</tr>"
                    + "<tr><th>Sorting:</th></tr>" 
                    + "<tr id='reportSorting'>" + "</tr>" 
                    + "<tr id='reportTableHeader'>" + "</tr>"
                + "</thead>"
                + "<tbody id='tablebody'></tbody>" 
            + "</table>");

        $.each(
            reportMetadatasMap[reportName].columns,
            function(key, value) {
                $('#reportTableHeader').append(
                    $("<td></td>").text(value.name));
                $('#reportFilters').append(
                    $("<th></th>").html(
                        buildFilter(value.name, value.type)));
                $('#reportSorting')
                    .append(
                    $("<th class='sorting'></th>"));
            });

        $(".sorting").each(
            function(index, value) {
                if (getTdHeaderName($(value)) === reportMetadatasMap[reportName].defaultSortColumn) {
                    $(value).html("Ascending");
                }
            });

        $('.sorting').on('click', function() {
            chooseSorting(this);
            queryForTableData(reportName);
        });

        $('#displayAreaSpan').append(
            $("<div class='well well-sm'><ul class='pagination'>" 
                + "<li class='page-item'><a class='page-link' id='decrease'><span aria-hidden='true'>&laquo;</span></a></li>"
                + "<li class='page-item'><span id='page'>0</span></li>"
                + "<li class='page-item'><a class='page-link' id='increase'><span aria-hidden='true'>&raquo;</span></a></li>"
            
            //just in case - if something goes wrong with autosend, we'll get it back 
            //+ "<div id='send'>send</div>"+
            
            + "</ul></div>"
             )
        );

        $('#decrease').on('click', function() {
            var pageIndex = parseInt($("#page").text(), 10);
            if (1 <= pageIndex) {
                $("#page").text(pageIndex - 1);
                queryForTableData(reportName);
            }
        });

		$('#decrease').hide();

        $('#increase').on('click', function() {
            var pageIndex = parseInt($("#page").text(), 10);
            $("#page").text(pageIndex + 1);
            queryForTableData(reportName);
        });

		$('.filterSubstring').on('input', function(){
			$("#page").text(0);
        	queryForTableData(reportName);
        });

		$('.filterTo').on('input', function(){
			$("#page").text(0);
        	queryForTableData(reportName);
        });
        
		$('.filterFrom').on('input', function(){
			$("#page").text(0);
        	queryForTableData(reportName);
        });
        
        //just in case - if something goes wrong with autosend, we'll get it back
        //$('#send').on('click', function(){
        //	queryForTableData(reportName);
        //});
        
        queryForTableData(reportName);
    }
}

$(document).ready(
    function() {
        reportMetadatasMap = [];

        for (i = 0; i < reportMetadatas.length; i++) {
            reportMetadatasMap[reportMetadatas[i].name] = reportMetadatas[i];
        }

        $.each(reportMetadatas, function(key, value) {
        	if (value.showInSelector) {
	        	$('#reportSelector').append([
	        		$("<li>").append($("<a>").attr("value", value.name).text(value.name)),
	        		("<li class='nav-divider'></li>")
	        	])
        	}
        });

        $.ajax({
            url : './testCookie',
            type : 'get',
            data : "1",
            success : function(data) {
                if (data.result == "success") {
                    showApp();
                    $('#username').text("Hello, " + data.username);
                } else {
                    showLoginForm();
                }
            }
        });

        $('#login-form').submit(function(e) {
            e.preventDefault();
            var username = $('#usernameInput').val();
            $.ajax({
                url : './login',
                type : 'post',
                data : $('#login-form').serialize(),
                success : function(data) {
                    if (data.result == "success") {
                        $('#username').text("Hello, " + username);
						$('#reportSelector li').first().css('background-color', 'white');
						chooseReport($('#reportSelector li').first().text());
						$("#tableHeader").text($('#reportSelector li').first().text());
                        showApp();
                    } else {
                        console.log("Failed login");
                        $('.validateInput').attr('style', "border-radius: 5px; border:#FF0000 1px solid;");
                    }
                }
            });
        });
        
        $('#reportSelector li').click(function() {
        	var selectedSidebarElement = $(this).text();
        	$('#reportSelector li').each(function() {
                if (!$(this).hasClass("nav-divider")) {
        			$(this).css('backgroundColor', '#f5f5f5');
        		}
        		else {
        			$(this).css('backgroundColor', '#e5e5e5;');
        		}
            });
        	$(this).css('background-color', 'white');
        	
        	chooseReport(selectedSidebarElement);
        	$("#tableHeader").text(selectedSidebarElement);
        });
        
        
        $('#reportSelector li').first().css('background-color', 'white');
        chooseReport($('#reportSelector li').first().text());
        $("#tableHeader").text($('#reportSelector li').first().text());
        
        console.log(reportMetadatasMap);
    }
);