function showLoginForm(){
	console.log("show login form triggered");
	$("#reportSelector").hide();
	$("#dataVisualisation").hide();
	$("#mainArea").hide();
	$('#login-container').show();
	$('#username').text("Not logged in");
}

function showApp(){
	$('#login-container').fadeOut('slow', function() {
		$("#mainArea").fadeIn(1000);
		$("#dataVisualisation").fadeIn(1000);
		$("#reportSelector").fadeIn(1000);
	});
}

$(document).ready(
	function() {	
		$.ajax({
		      type: "GET",
		      data: "1",
		      url: "./dbstatus",
		      dataType: "json",
		      success: function(resultData) {
		    	  $("#database-status").text(resultData.result);
		      }
		});
	}	
);