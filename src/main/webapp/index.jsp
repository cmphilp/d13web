<%@ page import="com.db.grad.d13.core.reports.ReportMetadatas" %>
<%@ page import="com.fasterxml.jackson.databind.ObjectMapper" %>

<!DOCTYPE html>
<html>    
    <head>
    	<title>dbAnalyser</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/login_register.css">
		<link rel="stylesheet" type="text/css" href="css/sidebar.css">
		<link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- JS Scripts -->
		<script type="text/javascript" src="js/login_form.js"></script>
		<script type="text/javascript" src="js/report_table.js"></script>
		<script src="https://d3js.org/d3.v4.min.js"></script>
    </head>
    <body> 	
    	<script>
    		var reportMetadatas = JSON.parse('<%= new ObjectMapper().writeValueAsString(ReportMetadatas.REPORT_METADATA) %>').reportMetadatas;
    	</script>
    	
    	<!-- Navbar -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-header">
				 <a href="#" class="pull-left"><img height="49.55" width="45.55" src="images/db_main_logo.png"></a>    
				<a class="navbar-brand" href="#">D1.3 dbAnalyser</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="nav-item"><a id="username">Not logged in</a></li>
					<li class="nav-item"><a id="database-status"></a></li>
					<li class="nav-item">
						<a href="#helpModal" aria-hidden="true" data-toggle="modal" data-target="#helpModal"><span class="glyphicon glyphicon-question-sign"></span></a>
					</li>
					<li class="nav-item"><a onclick="document.cookie = 'd13credentials' + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';location.reload();"><span class="glyphicon glyphicon-log-out">&nbsp;&nbsp;</span></a></li>
				</ul>
			</div>
		</nav>
    		
    	<!-- Modal Application Tutorial -->
  		<div class="modal fade helpModal" id="helpModal" role="dialog">
  			<div class="modal-dialog">
			    <div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal">&times;</button>
			        	<h4 class="modal-title">Application FAQ</h4>
			      	</div>
			      	<div class="modal-body">
			        	<div class="row">
			            	<div id="faq-div">
				                <div class="panel-group" id="accordion">
				                	<!-- Accordion 1 -->
				                    <div class="panel panel-default">
				                        <div class="panel-heading">
				                            <h4 class="panel-title">
				                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-1">
				                                    What is DBAnalyser?
				                                </a>
				                            </h4>
				                        </div>
				                        <div id="collapse-1" class="panel-collapse collapse">
				                            <div class="panel-body">
				                                <p>It is a data analysis tool designed by a team of Graduates at Deutsche Bank to simplify the 
				                                process of data visualisation currently employed by a demonstrative application. The application
				                                was designed over a 3 week period, and was managed using a SCRUM methodology.</p>
				                            </div>
				                        </div>
				                    </div>
				                    <!-- Accordion 2 -->
				                    <div class="panel panel-default">
				                        <div class="panel-heading">
				                            <h4 class="panel-title">
				                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-2">
				                                    How do I use it?
				                                </a>
				                            </h4>
				                        </div>
				                        <div id="collapse-2" class="panel-collapse collapse">
				                            <div class="panel-body">
				                                <p>Using the provided sidebar different data analysis can be presented, with options to filter data
				                                and sort based on specific columns. The pagination at the bottom of each table can be used to scroll
				                                through the data, and by clicking the links provided visual representations of the data can be seen.</p>
				                            </div>
				                        </div>
				                    </div>
				                    <!-- Accordion 3 -->
				                    <div class="panel panel-default">
				                        <div class="panel-heading">
				                            <h4 class="panel-title">
				                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-3">
				                                    How was it built?
				                                </a>
				                            </h4>
				                        </div>
				                        <div id="collapse-3" class="panel-collapse collapse">
				                            <div class="panel-body">
				                                <p>It is built using a combination of a Java backend, a MySQL database, and a combination of 
				                                Java Servlet Pages, CSS and JavaScript to present the application of the front-end. The 
				                                development of the project was driven by Test-Driven-Development (TDD).</p>
				                            </div>
				                        </div>
				                    </div>
				                    <!-- Accordion 4 -->
				                    <div class="panel panel-default">
				                        <div class="panel-heading">
				                            <h4 class="panel-title">
				                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-4">
				                                    What improvements are made over the original prototype?
				                                </a>
				                            </h4>
				                        </div>
				                        <div id="collapse-4" class="panel-collapse collapse">
				                            <div class="panel-body">
				                                <p>It features a design consisting of one single page, utilising jQuery to give the appearance
				                                of multiple pages. It features multiple graphical representations of data, using advanced SQL 
				                                queries to analyse correlations. It also features mobile-friendly, responsive UI design.</p>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
			      	</div>
			      	<div class="modal-footer">
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      	</div>
			    </div>
  			</div>
  		</div>
  		
		<!-- Login form -->
		<br/><br/><br/>
	    <div class="container" id="login-container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="panel panel-login">
						<div class="panel-heading">
							<h4>Welcome to the dbAnalyser! Please login to continue...</h4>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<form id="login-form" action="./login" method="post" role="form" style="display: block;">
										<div class="form-group">
											<input name="login" id="usernameInput" type="text" class="form-control validateInput" placeholder="Username" size=25 />
										</div>
										<div class="form-group">
											<input name="password" type="password" class="form-control validateInput" placeholder="Password" size=25 />
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6 col-sm-offset-3">
													<input type="submit" value="Login" class="form-control btn btn-register" />
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="dataVisualisation">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-sm-3 col-md-2 sidebar">
	    				<ul class="nav nav-sidebar" id="reportSelector" name="reportName">
	    					<!-- <li>Elements</li> -->
	    				</ul>
	    			</div>
	    			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" id="mainVisualContent">	
						<!-- Report Selector -->
						<h1 class="page-header" id="tableHeader"></h1>
						<div id="mainArea">
							<div class='table-responsive'>
							   	<span id="displayAreaSpan"></span>
							</div>    
						</div>
					</div>
				</div>
			</div>
		</div>
			    
	    <!-- Footer -->
	    <footer class="page-footer font-small blue pt-4">
			<div class="container-fluid text-center text-md-left">
				<div class="footer-copyright text-center py-3">
					� 2018 Copyright: D1.3 dbAnalyser
				</div>
			</div>
		</footer>
	</body>
</html>

